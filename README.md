# PGSQL backup
Ansible role to launch PostgreSQL backup / restore.

## Requirements

* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name         | Variable description        | Type    | Default | Required
---                   | ---                         | ---     | ---     | ---
pgsql_op              | pgsql op (backup / restore) | string  | no      | yes
pgsql_bckdir          | pgsql backup dir            | string  | yes     | yes
pgsql_dbname          | pgsql db name               | string  | no      | yes
pgsql_dbnametorestore | pgsql db name               | string  | no      | yes

## Dependencies

* psycopg2

## Playbook Example

```
---
- name: Load PostgreSQL backup role
  hosts: all
  become: yes
  vars_files:
    - vars/main.yml
  roles:
    - pgsql_backup
```

## License

GPL v3

## Links

* [PostgreSQL](https://www.postgresql.org/)
* [Ansible PosgreSQL modules](https://docs.ansible.com/ansible/latest/collections/community/postgresql/index.html)

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
